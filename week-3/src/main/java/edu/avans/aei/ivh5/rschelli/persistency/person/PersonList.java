/**
 * Avans Hogeschool Breda, Academie voor Engineering & ICT
 * Opleiding Informatica
 * 18 sep. 2014
 */
package edu.avans.aei.ivh5.rschelli.persistency.person;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author rschelli
 *
 */
public class PersonList implements Serializable {

	private ArrayList<Person> personList;
	
	public PersonList() {
		
		personList = new ArrayList<Person>();
	}
	
	public void addPerson(Person p) {
		personList.add(p);
	}
	
	public String toString() {
		String result = new String();
		
		for(Person p : personList) {
			result += "[" + p.toString() + "]"; 
		}
		return result;
	}
}
